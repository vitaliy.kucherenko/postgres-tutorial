create table car (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	make VARCHAR(100) NOT NULL,
	model VARCHAR(100) NOT NULL,
	price DECIMAL(8,2) NOT NULL CHECK ( price > 0 )
);

create table person (
	id BIGSERIAL NOT NULL PRIMARY KEY,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	email VARCHAR(150),
	gender VARCHAR(15) NOT NULL,
	country_of_birth VARCHAR(50) NOT NULL,
	date_of_birth DATE NOT NULL,
    car_id BIGINT REFERENCES car (id),
    UNIQUE(car_id)
);

insert into car (make, model, price) values ('Cadillac', 'XLR', 88974.39);
insert into car (make, model, price) values ('GMC', 'Yukon XL 1500', 70585.74);
insert into car (make, model, price) values ('Shelby', 'GT350', 55254.36);
insert into car (make, model, price) values ('Ford', 'GT', 89382.35);
insert into car (make, model, price) values ('Toyota', 'Celica', 37839.47);

insert into person (first_name, last_name, email, gender, country_of_birth, date_of_birth, car_id) values ('Rosy', 'Sleet', 'rsleet0@odnoklassniki.ru', 'Female', 'Norway', '2023-05-16', 1);
insert into person (first_name, last_name, email, gender, country_of_birth, date_of_birth, car_id) values ('Sheilah', 'Pittet', 'spittet1@miibeian.gov.cn', 'Female', 'China', '2023-03-25', 2);
insert into person (first_name, last_name, email, gender, country_of_birth, date_of_birth, car_id) values ('Wallie', 'Albiston', null, 'Polygender', 'Sweden', '2023-07-09', 3);
insert into person (first_name, last_name, email, gender, country_of_birth, date_of_birth, car_id) values ('Kamilah', 'Sievewright', 'ksievewright3@storify.com', 'Female', 'France', '2023-06-28', 4);
insert into person (first_name, last_name, email, gender, country_of_birth, date_of_birth, car_id) values ('Waneta', 'Gallatly', null, 'Female', 'Hungary', '2023-09-08', 5);