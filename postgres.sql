/* help */
\?

/* List of databases */
\l

/* Create database */
CREATE DATABASE test;

/* Connect to database */
\c test

/* Delete database */
DROP DATABASE test;

/* Create table */
CREATE TABLE person
(
    id            INT,
    first_name    VARCHAR(50),
    last_name     VARCHAR(50),
    gender        VARCHAR(6),
    date_of_birth TIMESTAMP
);

/* List of relations */
\d
\dt

/* Table */

\d person

/* Create table */
DROP TABLE person;


/* Create table */
CREATE TABLE person
(
    id            BIGSERIAL   NOT NULL PRIMARY KEY,
    first_name    VARCHAR(50) NOT NULL,
    last_name     VARCHAR(50) NOT NULL,
    gender        VARCHAR(6)  NOT NULL,
    date_of_birth TIMESTAMP
);

/* Insert records into table */
INSERT INTO person (first_name, last_name, gender, date_of_birth)
VALUES ('Vitalii', 'K', 'MALE', '1995-03-19');

/* Select from table */
SELECT *
FROM person;

/* execute commands from file */
\i /person.sql

/*
order
1 2 3 4 ASC
4 3 2 1 DESC
*/
SELECT id, first_name, date_of_birth
FROM person
ORDER BY first_name, date_of_birth DESC;

/* DISTINCT */
SELECT DISTINCT country_of_birth
FROM person
ORDER BY country_of_birth;

/* Where Clause and AND */
SELECT *
FROM person
WHERE gender = 'Male';
SELECT *
FROM person
WHERE gender = 'Male'
  AND country_of_birth = 'Indonesia';
SELECT *
FROM person
WHERE gender = 'Male'
  AND (country_of_birth = 'Indonesia' OR country_of_birth = 'China');
SELECT *
FROM person
WHERE gender = 'Male'
  AND (country_of_birth = 'Indonesia' OR country_of_birth = 'China')
  AND last_name = 'Ferries';

/* Comparison Operators */
SELECT 1 = 1;
SELECT 1 = 2;
SELECT 1 < 2;
SELECT 1 <> 2;

/* Limit, Offset & Fetch */
SELECT *
FROM person
LIMIT 10;
SELECT *
FROM person
OFFSET 5 LIMIT 5;
SELECT *
FROM person
OFFSET 5 FETCH FIRST 5 ROW ONLY;
SELECT *
FROM person
OFFSET 5 FETCH FIRST 2 ROW ONLY;

/* IN */
SELECT *
FROM person
WHERE country_of_birth = 'Guatemala'
   OR country_of_birth = 'China';
SELECT *
FROM person
WHERE country_of_birth IN ('Guatemala', 'China')
ORDER BY country_of_birth;

/* Between */
SELECT *
FROM person
WHERE date_of_birth BETWEEN DATE '2023-01-01' AND '2023-02-28';

/* Like And iLike */
SELECT id, email
FROM person;
SELECT id, email
FROM person
WHERE email LIKE '%.com';
SELECT id, email
FROM person
WHERE email LIKE '%@google%';
SELECT id, email
FROM person
WHERE email LIKE '______@%';
SELECT id, country_of_birth
FROM person
WHERE country_of_birth ILIKE 'p%';

/* Group By */
SELECT DISTINCT country_of_birth
FROM person;
SELECT country_of_birth, COUNT(*)
FROM person
GROUP BY country_of_birth
ORDER BY country_of_birth;

/* Group By Having */
SELECT DISTINCT country_of_birth
FROM person;
SELECT country_of_birth, COUNT(*)
FROM person
GROUP BY country_of_birth
HAVING COUNT(*) > 50
ORDER BY country_of_birth;

/* Calculating Min, Max & Average */
SELECT *
FROM car;
SELECT MAX(price)
FROM car;
SELECT MIN(price)
FROM car;
SELECT AVG(price)
FROM car;
SELECT ROUND(AVG(price))
FROM car;
SELECT make, model, MIN(price)
FROM car
GROUP BY make, model
ORDER BY make;
SELECT make, MIN(price)
FROM car
GROUP BY make
ORDER BY make;

/* Sum */
SELECT SUM(price)
FROM car;
SELECT make, SUM(price)
FROM car
GROUP BY make
ORDER BY make;
SELECT make, SUM(price)
FROM car
WHERE make = 'Acura'
GROUP BY make;

/* Arithmetic Operators (ROUND) */
SELECT id, make, model, price, ROUND(price * .10, 2)
FROM car;
SELECT id,
       make,
       model,
       price,
       ROUND(price * .10, 2)           AS ten_percent,
       ROUND(price - (price * .10), 2) AS discount_ten_percent
FROM car;

/* Coalesce */
SELECT COALESCE(email, 'Email not provided')
FROM person;

/* NULLIF */
SELECT NULLIF(10, 10);
SELECT NULLIF(10, 19);
SELECT 10 / NULLIF(0, 0);
SELECT COALESCE(10 / NULLIF(0, 0), 0);

/* Timestamps And Dates Course */
SELECT NOW();
SELECT NOW()::DATE;
SELECT NOW()::TIME;
SELECT NOW() - INTERVAL '1 YEAR';
SELECT (NOW() - INTERVAL '10 MONTHS')::DATE;
SELECT EXTRACT(DAY FROM NOW());

/* Age Function */
SELECT first_name, last_name, gender, country_of_birth, date_of_birth, AGE(NOW(), date_of_birth) AS age
FROM person;

/* Unique Constraints */
ALTER TABLE person
    ADD CONSTRAINT unique_email_address UNIQUE (email);
SELECT email, COUNT(*)
FROM person
GROUP BY email
HAVING COUNT(*) > 1;
ALTER TABLE person
    DROP CONSTRAINT unique_email_address;

/* Check Constraints */
ALTER TABLE person
    ADD CONSTRAINT gender_constrain CHECK (gender = 'Female' OR gender = 'Male');
SELECT DISTINCT gender
FROM person;
DELETE
FROM person
WHERE gender = 'Genderqueer'
   OR gender = 'Bigender'
   OR gender = 'Genderfluid';
DELETE
FROM person
WHERE gender = 'Non-binary'
   OR gender = 'Polygender'
   OR gender = 'Agender';
INSERT INTO person (first_name, last_name, email, gender, country_of_birth, date_of_birth)
VALUES ('Rosy', 'Sleet', 'rsleet0123@odnoklassniki.ru', 'TEST', 'Norway', '2023-05-16');

/* Delete Records */
DELETE
FROM person;
DELETE
FROM person
WHERE id = '2';
DELETE
FROM person
WHERE gender = 'Female';

/* Update Records */
SELECT *
FROM person
WHERE id = '1';
UPDATE person
SET last_name = 'Sleet Sleet'
WHERE id = '1';

/* On Conflict Do Nothing */
SELECT *
FROM person
WHERE id = '1';
INSERT INTO person (id, first_name, last_name, email, gender, country_of_birth, date_of_birth)
VALUES (1, 'Rosy 2', 'Sleet 2', 'rsleet0@odnoklassniki.ru', 'Female', 'Norway', '2023-05-16')
ON CONFLICT (id) DO NOTHING;

/* Upsert */
SELECT *
FROM person
WHERE id = '1';
INSERT INTO person (id, first_name, last_name, email, gender, country_of_birth, date_of_birth)
VALUES (1, 'Rosy 2', 'Sleet 2', 'rsleet0@odnoklassniki.ru', 'Female', 'Norway', '2023-05-16')
ON CONFLICT (id) DO UPDATE SET email = 'rsleet0@gmail.com';
INSERT INTO person (id, first_name, last_name, email, gender, country_of_birth, date_of_birth)
VALUES (1, 'Rosy 2', 'Sleet 2', 'rsleet0@odnoklassniki.ru', 'Female', 'Norway', '2023-05-16')
ON CONFLICT (id) DO UPDATE SET first_name = EXCLUDED.first_name,
                               last_name  = EXCLUDED.last_name;

/* INNER JOIN */
SELECT *
FROM person;
SELECT *
FROM car;
SELECT *
FROM person
         JOIN car ON person.car_id = car.id;
SELECT person.last_name, car.make
FROM person
         JOIN car ON person.car_id = car.id;

/* LEFT JOIN */
SELECT *
FROM person;
SELECT *
FROM car;
SELECT *
FROM person
         LEFT JOIN car ON car.id = person.car_id;

/* Exporting Query Results to CSV */
\copy (SELECT * FROM person LEFT JOIN car ON car.id = person.car_id) TO '/test.csv' DELIMITER ',' CSV HEADER;

/* Serial & Sequences */
SELECT *
FROM person;
SELECT *
FROM person_id_seq;
SELECT nextval('person_id_seq'::regclass);
insert into person (first_name, last_name, email, gender, country_of_birth, date_of_birth, car_id)
values ('Wilfred', 'Wehnerr', null, 'Male', 'China', '2023-03-21', 5);
ALTER SEQUENCE person_id_seq RESTART WITH 8;

/* UUID */
select *
from pg_available_extensions;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
\df
SELECT uuid_generate_v4();
SELECT *
FROM person;
SELECT *
FROM car;
UPDATE person
SET car_uid = '875bb829-7ebc-4d58-bf3a-9afdaba3751c'
where person_uid = '8b9833d9-4c3d-4822-8661-7b02e739b2c1';
SELECT *
FROM person
         JOIN car USING (car_uid);
SELECT *
FROM person
         LEFT JOIN car USING (car_uid)
WHERE car.* IS NULL;


/* Создание таблиц и вставка данных из запроса */
SELECT *
FROM person;
CREATE TABLE person_f AS
SELECT *
FROM person
WHERE gender = 'Female';
SELECT *
FROM person_f;
CREATE TABLE person_m AS
SELECT first_name, last_name, gender
FROM person
WHERE gender = 'Male';
SELECT *
FROM person_m;
CREATE TABLE person_2 AS
SELECT *
FROM person
WHERE 2 = 1; /* не наполнять данными */
CREATE TABLE person_3 AS
SELECT first_name, last_name, gender
FROM person
WHERE 2 = 1; /* не наполнять данными */
SELECT *
FROM person_2;
SELECT *
FROM person_3;

INSERT INTO person_2 (список полей)
SELECT список полей
FROM person
WHERE;

INSERT INTO person_2
SELECT *
FROM person
WHERE gender = 'Female';
INSERT INTO person_3(first_name, last_name, gender)
SELECT first_name, last_name, gender
FROM person
WHERE gender = 'Male';

/*  Pivot, переворачиваем запрос с группировкой */
/*  Pivot - в Oracle */
CREATE TABLE t1
(
    id   BIGSERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(20),
    prop VARCHAR(20)
);

INSERT INTO t1
VALUES (1, 'Круг', 'зеленый');
INSERT INTO t1
VALUES (2, 'Круг', 'зеленый');
INSERT INTO t1
VALUES (3, 'Круг', 'Красный');
INSERT INTO t1
VALUES (4, 'Круг', 'Синий');
INSERT INTO t1
VALUES (5, 'Квадрат', 'зеленый');
INSERT INTO t1
VALUES (6, 'Квадрат', 'Красный');
INSERT INTO t1
VALUES (7, 'Квадрат', 'Синий');
INSERT INTO t1
VALUES (8, 'Ромб', 'Красный');
INSERT INTO t1
VALUES (9, 'Ромб', 'Синий');
SELECT *
FROM t1;
SELECT name, prop, COUNT(prop) count
FROM t1
GROUP BY name, prop;
SELECT name, COUNT(name) count
FROM t1
GROUP BY name;

SELECT name,
       SUM(CASE WHEN prop = 'Синий' THEN 1 ELSE 0 END)   AS "Синий",
       SUM(CASE WHEN prop = 'Красный' THEN 1 ELSE 0 END) AS "Красный",
       SUM(CASE WHEN prop = 'зеленый' THEN 1 ELSE 0 END) AS "зеленый"
FROM t1
GROUP BY name;

/**/

CREATE TABLE client
(
    id         BIGSERIAL NOT NULL PRIMARY KEY,
    name       VARCHAR(255),
    created_at TIMESTAMP,
    active     bool default true
);

INSERT INTO client (name, active)
SELECT CONCAT('Gen Cli', idx), (random() > 0.5)
FROM generate_series(1, 5000000) as idx;

/* EXPLAIN */
ALTER TABLE client
    ADD balance INTEGER;
SELECT *
FROM client
LIMIT 10;
EXPLAIN
SELECT *
FROM client;
EXPLAIN
SELECT *
FROM client
WHERE active = true;
-- Seq Scan on client  (cost=0.00..118653.01 rows=2496165 width=35)
EXPLAIN
SELECT *
FROM client
WHERE active = false
  and id = 10;
-- Index Scan using client_pkey on client  (cost=0.43..8.45 rows=1 width=35)
EXPLAIN
SELECT *
FROM client
WHERE id in (1, 2, 3, 4, 5);

UPDATE client
SET balance = FLOOR(random() * 100) + 1;

EXPLAIN
SELECT *
FROM client
WHERE client.balance = 30;

EXPLAIN
SELECT *
FROM client
WHERE name = 'Gen Cli2';
-- Gather  (cost=1000.00..95685.19 rows=1 width=35)
-- Index Scan using client_name_hash_index on client  (cost=0.00..8.02 rows=1 width=35) / hash
-- Index Scan using client_name_hash_index on client  (cost=0.43..8.45 rows=1 width=35)
CREATE INDEX client_balance_index ON client (balance);
CREATE INDEX client_active_index ON client (active);
CREATE INDEX client_name_hash_index ON client USING HASH (name);
CREATE INDEX client_name_index ON client (name);
DROP INDEX client_name_hash_index;

EXPLAIN (ANALYSE, COSTS OFF, TIMING OFF)
SELECT *
FROM client;
EXPLAIN (ANALYSE, COSTS OFF, TIMING OFF)
SELECT *
FROM client
WHERE id = 10;
EXPLAIN (ANALYSE, COSTS OFF, TIMING OFF)
SELECT *
FROM client
WHERE name = 'Gen Cli2';
-- Planning Time: 0.065 ms
-- Execution Time: 182.116 ms
-- add index
-- Planning Time: 0.149 ms
-- Execution Time: 0.018 ms

/* Подготовленый оператор для запроса */
PREPARE client_name(varchar) AS SELECT *
                                FROM client
                                WHERE name = $1;
EXECUTE client_name('Gen Cli2'); -- 245ms без индекса / 46ms с индексом
SELECT *
FROM client
WHERE name = 'Gen Cli2';
-- 547ms без индекса / 55ms с индексом

-- все подготовленные операторы можно увидеть в представлении pg_prepared_statements
SELECT *
FROM pg_prepared_statements;

-- удалить оператор DEALLOCATE
DEALLOCATE client_name;

/* Курсоры */
BEGIN;
DECLARE c CURSOR FOR SELECT *
                     FROM client;
FETCH c;
CLOSE c;
COMMIT;

-- Планировщик не будет рассматривать параллельные планы для таблиц размером меньше, чем
CREATE EXTENSION IF NOT EXISTS pgstattuple;
SELECT current_setting('min_parallel_table_scan_size');

SELECT pg_size_pretty(pg_table_size('client')) AS size, (SELECT count(*) FROM client) AS cnt;

--
EXPLAIN
SELECT count(*)
FROM client;


/* */
EXPLAIN (ANALYSE, COSTS OFF, TIMING OFF)
SELECT *
FROM client
WHERE name = 'Gen Cli2';
EXPLAIN (ANALYSE, COSTS OFF, TIMING OFF)
SELECT name
FROM client
WHERE name = 'Gen Cli3';
EXPLAIN (ANALYSE, COSTS OFF, TIMING OFF)
SELECT *
FROM client
WHERE id = 10;
EXPLAIN (ANALYSE, COSTS OFF, TIMING OFF)
SELECT id
FROM client
WHERE id <= 10000;

SELECT vacuum_count, autovacuum_count
FROM pg_stat_all_tables
WHERE relname = 'client';

VACUUM client;

--
EXPLAIN
SELECT *
FROM client;
EXPLAIN
SELECT *
FROM client
ORDER BY id;
EXPLAIN
SELECT *
FROM client
ORDER BY id
LIMIT 100;
EXPLAIN
SELECT *
FROM client
WHERE id = 10
ORDER BY id;

--
SELECT now() - INTERVAL '7 months' AS d
\gset;
SELECT count(*)
from person
WHERE date_of_birth > :'d';
SELECT count(*)
from person
WHERE date_of_birth > (SELECT now() - INTERVAL '7 months');

--
SELECT count(*)
FROM person;
SELECT *
FROM person
LIMIT 10;
SELECT p.last_name, c.make, count(p.car_id)
FROM person p
         JOIN car c ON p.car_id = c.id
GROUP BY p.last_name, c.make, p.car_id
HAVING COUNT(p.car_id) > 1;
SELECT *
FROM car
LIMIT 10;

SELECT p1.id         AS id1,
       p1.first_name AS first_name1,
       p1.last_name  AS last_name1,
       p2.id         AS id2,
       p2.first_name AS first_name2,
       p2.last_name  AS last_name2,
       c.make,
       c.model
FROM person p1
         JOIN
     car c ON p1.car_id = c.id
         JOIN
     person p2 ON p1.car_id = p2.car_id AND p1.id <> p2.id
ORDER BY c.make,
         c.model;

SELECT c.make,
       c.model,
       COUNT(*) AS count_of_people_with_same_car
FROM person p
         JOIN
     car c ON p.car_id = c.id
GROUP BY c.make, c.model
HAVING COUNT(*) > 1
ORDER BY count_of_people_with_same_car DESC;

SELECT p.last_name, c.model
FROM person p
         JOIN car c ON p.car_id = c.id
ORDER BY p.car_id
LIMIT 10;
EXPLAIN
SELECT p.last_name, c.model
FROM person p
         JOIN car c ON p.car_id = c.id
ORDER BY p.car_id
LIMIT 10;
EXPLAIN (COSTS OFF)
SELECT *
FROM person p
         JOIN car c ON p.car_id = c.id;

CREATE INDEX person_car_index ON person (car_id); -- ни чего не изменилось
DROP INDEX person_car_index;
EXPLAIN (ANALYSE, COSTS OFF)
SELECT *
FROM person p
         JOIN car c ON p.car_id = c.id
WHERE p.id in (10, 101);

SELECT c.*
FROM person p
         JOIN car c ON c.id != p.id;

-- хеширование
-- work_mem
-- temp_file_limit - дисковая память
EXPLAIN
SELECT *
FROM person p
         JOIN car c ON p.car_id = c.id;
SELECT make, count(*)
FROM car
GROUP BY make;
EXPLAIN
SELECT make, count(*)
FROM car
GROUP BY make;
SHOW work_mem;
SET work_mem = '128MB';

-- MERGE JOIN
/*
В PostgreSQL оператор соединения слиянием представлен как MERGE JOIN. Этот оператор используется для объединения двух
или более таблиц на основе условия соединения и обеспечивает эффективное выполнение запросов в случае, когда
соединяемые таблицы отсортированы по столбцу условия соединения.
Для того чтобы PostgreSQL использовал оператор слияния (MERGE JOIN), обе таблицы должны быть отсортированы по столбцу,
который вы используете в условии соединения. Вы можете убедиться, что PostgreSQL использует слияние, взглянув на план
выполнения запроса.

Пример использования слияния:
    SELECT *
    FROM table1
    JOIN table2 ON table1.column1 = table2.column2;
Если обе таблицы отсортированы по column1 и column2 соответственно, то PostgreSQL может выбрать слияние для выполнения
запроса.

Однако важно помнить, что PostgreSQL автоматически выбирает метод соединения на основе статистики и структуры таблицы,
и в большинстве случаев вы можете полагаться на его оптимизатор запросов для выбора наиболее эффективного метода
выполнения запроса.
 */

-- Статистика
show default_statistics_target ; -- 100
set default_statistics_target = 10;
ANALYZE;
EXPLAIN select * from client;
select count(*) from client;
select * from client limit 10;
SELECT * FROM pg_stats s WHERE s.tablename = 'client' AND s.attname = 'balance';
EXPLAIN SELECT * FROM client WHERE client.balance < 10;
SELECT * FROM client WHERE client.balance < 10;

-- ПРОФИЛИРОВАНИЕ
--pg_stat_statements
--EXPLAIN ANALYSE
CREATE EXTENSION pg_stat_statements;
ALTER SYSTEM SET shared_preload_libraries = 'pg_stat_statements';
-- restart system
SET pg_stat_statements.track = 'all';
-- Создаем представление (была ошибка с полем total_time, в таблице pg_stat_statements ее нет, использовал вместо ее
-- total_exec_time)

CREATE VIEW statements_v AS SELECT substring(regexp_replace(query, ' +', ' ', 'g') FOR 55) AS query,
                                   calls, round(total_exec_time)/1000 AS time_sec,
                                   shared_blks_hit + shared_blks_read + shared_blks_written AS shared_blks
FROM pg_stat_statements
ORDER BY total_exec_time DESC;

SELECT query, calls, total_exec_time, rows, 100.0 * shared_blks_hit /
               nullif(shared_blks_hit + shared_blks_read, 0) AS hit_percent
          FROM pg_stat_statements ORDER BY total_exec_time DESC LIMIT 5;

-- Сбросить статистику
SELECT pg_stat_statements_reset();
SELECT count(*) FROM person p JOIN car c ON p.car_id = c.id WHERE c.make = 'Ford';
SELECT * FROM statements_v \gx;

SELECT make, count(*) FROM car GROUP BY make HAVING count(*) > 1 ORDER BY make;
EXPLAIN SELECT
    make,
    COUNT(*) AS count,
    STRING_AGG(CAST(id AS TEXT), ', ') AS id_list
FROM
    car
GROUP BY
    make
HAVING
    COUNT(*) > 1
ORDER BY
    make;


-- отслеживание планов запросов активных сеансов

CREATE EXTENSION IF NOT EXISTS "pg_query_state"; -- ERROR: could not open extension control file "/usr/share/postgresql/14/extension/pg_query_state.control": No such file or directory
LOAD 'pg_hint_plan';  -- не работает
CREATE EXTENSION pg_hint_plan;  -- не работает
EXPLAIN SELECT count(*) FROM client;
/*
 Finalize Aggregate  (cost=95855.43..95855.44 rows=1 width=8)
  ->  Gather  (cost=95855.21..95855.42 rows=2 width=8)
        Workers Planned: 2
        ->  Partial Aggregate  (cost=94855.21..94855.22 rows=1 width=8)
              ->  Parallel Seq Scan on client  (cost=0.00..89610.77 rows=2097777 width=0)
 */

/*+ Parallel(client 0 hard) */
EXPLAIN SELECT count(*) FROM client; -- не работает

/* Parallel(client 3 hard) */
EXPLAIN SELECT count(*) FROM client; -- не работает
SET parallel_workers = 3;  -- не работает
SHOW max_parallel_workers;


