SELECT *
FROM person
LIMIT 10;
ALTER TABLE person
    add pib text;
UPDATE person
set pib = concat(last_name, ' ', first_name);

CREATE OR REPLACE FUNCTION public.update_person_pib()
    RETURNS trigger AS
$BODY$
BEGIN
    NEW.pib = CONCAT(NEW.last_name, ' ', NEW.first_name);
    RETURN NEW;
END;
$BODY$
    LANGUAGE plpgsql VOLATILE
                     COST 100;

CREATE TRIGGER "update_person_pib_on_insert"
    BEFORE INSERT
    ON person
    FOR EACH ROW
EXECUTE PROCEDURE "update_person_pib"();

INSERT INTO public.person (id, first_name, last_name, email, gender, country_of_birth, date_of_birth, car_id)
VALUES (1002, 'LLL', 'BBB', 'ebarefootdu@sohu.com', 'Male', 'Zimbabwe', '2022-01-22', 1);
SELECT *
FROM person
WHERE id = 1002;

-- плохая практика
CREATE TRIGGER "update_person_pib_on_update"
    BEFORE UPDATE
    ON person
    FOR EACH ROW
EXECUTE PROCEDURE "update_person_pib"();
-- лучший вариант
CREATE TRIGGER "update_person_pib_on_update"
    BEFORE UPDATE OF first_name, last_name ON person
    FOR EACH ROW WHEN ( (NEW.first_name != OLD.first_name) or (NEW.last_name != OLD.last_name) )
    EXECUTE PROCEDURE "update_person_pib"();

UPDATE person SET first_name = 'Latinos' WHERE id = 1002;
