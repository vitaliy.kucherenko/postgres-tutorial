create table car (
	car_uid UUID NOT NULL PRIMARY KEY,
	make VARCHAR(100) NOT NULL,
	model VARCHAR(100) NOT NULL,
	price DECIMAL(8,2) NOT NULL CHECK ( price > 0 )
);

create table person (
	person_uid UUID NOT NULL PRIMARY KEY,
	first_name VARCHAR(50) NOT NULL,
	last_name VARCHAR(50) NOT NULL,
	email VARCHAR(150),
	gender VARCHAR(15) NOT NULL,
	country_of_birth VARCHAR(50) NOT NULL,
	date_of_birth DATE NOT NULL,
    car_uid UUID REFERENCES car (car_uid),
    UNIQUE(car_uid)
);

insert into car (car_uid, make, model, price) values (uuid_generate_v4(), 'Cadillac', 'XLR', 88974.39);
insert into car (car_uid, make, model, price) values (uuid_generate_v4(), 'GMC', 'Yukon XL 1500', 70585.74);
insert into car (car_uid, make, model, price) values (uuid_generate_v4(), 'Shelby', 'GT350', 55254.36);
insert into car (car_uid, make, model, price) values (uuid_generate_v4(), 'Ford', 'GT', 89382.35);
insert into car (car_uid, make, model, price) values (uuid_generate_v4(), 'Toyota', 'Celica', 37839.47);

insert into person (person_uid, first_name, last_name, email, gender, country_of_birth, date_of_birth) values (uuid_generate_v4(), 'Rosy', 'Sleet', 'rsleet0@odnoklassniki.ru', 'Female', 'Norway', '2023-05-16');
insert into person (person_uid, first_name, last_name, email, gender, country_of_birth, date_of_birth) values (uuid_generate_v4(), 'Sheilah', 'Pittet', 'spittet1@miibeian.gov.cn', 'Female', 'China', '2023-03-25');
insert into person (person_uid, first_name, last_name, email, gender, country_of_birth, date_of_birth) values (uuid_generate_v4(), 'Wallie', 'Albiston', null, 'Polygender', 'Sweden', '2023-07-09');
insert into person (person_uid, first_name, last_name, email, gender, country_of_birth, date_of_birth) values (uuid_generate_v4(), 'Kamilah', 'Sievewright', 'ksievewright3@storify.com', 'Female', 'France', '2023-06-28');
insert into person (person_uid, first_name, last_name, email, gender, country_of_birth, date_of_birth) values (uuid_generate_v4(), 'Waneta', 'Gallatly', null, 'Female', 'Hungary', '2023-09-08');